//
//  CustomTableViewCell.h
//  TestCellHeightWithTextView
//
//  Created by Jose Catala on 31/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
