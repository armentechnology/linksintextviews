//
//  CustomTableViewCell.m
//  TestCellHeightWithTextView
//
//  Created by Jose Catala on 31/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
