//
//  CustomTextView.h
//  TestCellHeightWithTextView
//
//  Created by Jose Catala on 31/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CustomTextView : UITextView

@property IBInspectable BOOL adjustView;

@end
