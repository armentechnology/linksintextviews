//
//  CustomTextView.m
//  TestCellHeightWithTextView
//
//  Created by Jose Catala on 31/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "CustomTextView.h"

@interface CustomTextView()<UITextViewDelegate>
@end

@implementation CustomTextView

@synthesize adjustView = _adjustView;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) dealloc
{
    self.delegate = nil;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    if (self.adjustView)  [self textViewFitToContent:self];
    
    self.delegate = self;
}

- (void) textViewDidChangeSelection:(UITextView *)textView
{
    if(!NSEqualRanges(textView.selectedRange, NSMakeRange(0, 0))) {
        textView.selectedRange = NSMakeRange(0, 0);
    }
}

- (void)addGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    // remove magnifying glass, still allowing link detection:
    if ([gestureRecognizer class] == [UILongPressGestureRecognizer class] || [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        [super addGestureRecognizer:gestureRecognizer];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // remove magnifying glass, still allowing link detection:
    if ([gestureRecognizer class] == [UILongPressGestureRecognizer class] || [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

#pragma mark RESIZE TEXTVIEW TO ITS CONTENT

- (void)textViewFitToContent:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    textView.scrollEnabled = NO;
}

@end
