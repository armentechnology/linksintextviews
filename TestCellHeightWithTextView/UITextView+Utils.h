//
//  UITextView+Utils.h
//  morethan
//
//  Created by Jose Catala on 08/08/2018.
//  Copyright © 2018 Trak. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SCREEN_HEIGHT  [UIScreen mainScreen].bounds.size.height

@interface UITextView (Utils)

+ (NSAttributedString *) htlmToAttributedString:(NSString *)htmlString;


@end
