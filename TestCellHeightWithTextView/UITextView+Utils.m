//
//  UITextView+Utils.m
//  morethan
//
//  Created by Jose Catala on 08/08/2018.
//  Copyright © 2018 Trak. All rights reserved.
//

#import "UITextView+Utils.h"

@implementation UITextView (Utils)

+ (NSAttributedString *) htlmToAttributedString:(NSString *)htmlString
{
    //Converting HTML string with UTF-8 encoding to NSAttributedString
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding) }
                                            documentAttributes: nil
                                            error: nil ];
    return attributedString;
}

@end
